from flask import request, jsonify, Flask
import os
from .backend.services import CallService

app = Flask(__name__)

@app.route("/signup", methods['POST'])
def signup():
    user_data = request.json
    user = create(user_data)
    return user, 201
@app.route("/login")
def login():
    return {}

@app.route("/profile/<int:user_id>")
def update_user():
    return {}

@app.route("/profile/<int:user_id>")
def delete_user():
    return {}
@app.route("/users")
def find_all_characters():
    # if not os.path.exists(FILE_PATH) or os.stat(FILE_PATH).st_size == 0:
    #     return {}
    return jsonify(CallService.all_users())